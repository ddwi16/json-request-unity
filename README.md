# JSON Request Unity

## About Author
Name    : Dicky Dwi Darmawan <br>
NRP     : 4210181028 <br>
D4 Teknologi Game - Politeknik Elektronika Negeri Surabaya <br>

## Code
In this project we have 2 separate script <br> 
~ one for hold the JSON data component, You can follow the script from here [JsonDataClass.cs](https://gitlab.com/ddwi16/json-request-unity/-/blob/master/Assets/Scripts/JsonDataClass.cs) <br>
~ and the another is to control JSON data, so we can implement that to Unity UI as a text. You can follow the script from here [BubbleText.cs](https://gitlab.com/ddwi16/json-request-unity/-/blob/master/Assets/Scripts/BubbleText.cs) <br>

## Here The Result
![](/Assets/JSON-request.gif)

## Description
The `JsonDataClass.cs` is the script represent for the JSON component data who hold the id, name, avatar, and email. Based on the JSON component data, we requested from [here](https://5e510330f2c0d300147c034c.mockapi.io/users). <br> 
The `BubbleText.cs` is the script who control the JSON component data, so after we request the data from the link i put before, we convert it as object type on C# scripts on Unity, also we can manipulate the data we got, so we can change the value text, or we can display the value data component from the JSON we requested.
The following progress is like this :
1. get request from url JSON data with WWW request Unity.
2. turn it into object type with `JsonDataClass.cs`, where class JsonDataClass for component the JSON data, and the class JsonDataCollection for the list array id from the data.
3. convert the data to text UI Unity.
