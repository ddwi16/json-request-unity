﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleText : MonoBehaviour
{
    [SerializeField]
    private string jsonURL;
    public Text nama_email;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(getJsonData());
    }

    IEnumerator getJsonData() {
        //get request from url
        WWW jsonReq = new WWW(jsonURL);
        yield return jsonReq;

        //get data complete and return as string
        var jsonText = jsonReq.text;
        JsonDataCollection jsonDataClass = JsonUtility.FromJson<JsonDataCollection>("{\"ids\":" + jsonText + "}");

        //convert it to text UI unity
        nama_email.text = "Nama : " + jsonDataClass.ids[27].name + "\n" + "Email : " + jsonDataClass.ids[27].email;
    }
}
