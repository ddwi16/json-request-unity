﻿using System;
using System.Collections.Generic;

[Serializable]
public class JsonDataClass
{
    public int id;
    public string name; 
    public string avatar; 
    public string email;
}

[Serializable]
public class JsonDataCollection {
    public JsonDataClass[] ids;
}